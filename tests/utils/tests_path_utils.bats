#!/usr/bin/env bats

source "./src/utils/path_utils.sh"

@test "It should extract the path name from the record" {
  # Given
  record="Contenido: /path 1/path 2/path 3/ (10 MegaBaits)"
  expected_path="/path 1/path 2/path 3/"
  
  # When
  actual_path="$(extract_pathname "$record")"

  # Then
  [ "$actual_path" = "$expected_path" ]
}