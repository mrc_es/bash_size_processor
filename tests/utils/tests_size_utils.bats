#!/usr/bin/env bats

source "./src/utils/size_utils.sh"

@test "It should normalize the size suffix" {
  # Given
  bad_suffix="mEgaBites"
  expected_suffix="M"
  
  # When
  result_suffix="$(normalize_size_suffix "$bad_suffix")"

  # Then
  [ "$result_suffix" = "$expected_suffix" ]
}

@test "It should normalize the size to bytes" {
  # Given
  kb_size="10K"
  mb_size="10M"
  tb_size="10T"

  expected_kb_size="10000"
  expected_mb_size="10000000"
  expected_tb_size="10000000000000"
  
  # When
  result_kb_size="$(normalize_size_to_bytes "$kb_size")"
  result_mb_size="$(normalize_size_to_bytes "$mb_size")"
  result_tb_size="$(normalize_size_to_bytes "$tb_size")"

  # Then
  [ "$result_kb_size" -eq "$expected_kb_size" ]
  [ "$result_mb_size" -eq "$expected_mb_size" ]
  [ "$result_tb_size" -eq "$expected_tb_size" ]
}

@test "It should extract the size part from the record" {
  # Given
  record="stuff /path1/path2/   (34TeRaBites)"
  expected_size_part="34TeRaBites"
  
  # When
  actual_size_part="$(extract_size_part "$record")"

  # Then
  [ "$actual_size_part" = "$expected_size_part" ]
}

@test "It should extract the size number from the size part" {
  # Given
  size_part="90Kilobaits"
  expected_size_number=90
  
  # When
  actual_size_number="$(extract_size_number "$size_part")"

  # Then
  [ "$actual_size_number" -eq "$expected_size_number" ]
}
