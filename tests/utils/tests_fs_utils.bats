#!/usr/bin/env bats

source "./src/utils/fs_utils.sh"
source "./src/utils/size_utils.sh"

@test "It should extract the filesystem name" {
  # Given
  record="FS : /archivos (100 T)"
  expected_fs_name="/archivos"
  
  # When
  actual_fs_name="$(extract_fsname "$record")"

  # Then
  [ "$actual_fs_name" = "$expected_fs_name" ]
}

@test "It should print filesystem info" {
  # Given
  test_first_record="FS : /archivos (100 T)"
  test_percentage_occupied=10

  expected_fs_info="/archivos -> Ocupa 100T, ocupado 10%"
  
  # When
  actual_fs_info="$(print_fs_info "$test_first_record" "$test_percentage_occupied")"

  # Then
  [ "$actual_fs_info" = "$expected_fs_info" ]
}
