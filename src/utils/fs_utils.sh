#!/usr/env/bin bash

extract_fsname() {
    local -r record="$1"
    local -r fs_name_regex='(?<=\/)\w+(?=\s*\()'
    
    local -r fs_name_match="$(echo "$record" | grep -Po "$fs_name_regex")"
    local -r fs_name="/$fs_name_match"
    echo "$fs_name"
}
print_fs_info() {
    local -r first_row="$1"
    local -ri percentage_occupied="$2"

    local -r fs_name="$(extract_fsname "$first_row")"
    local -r size_with_suffix="$(create_size_with_suffix_normalized "$first_row")"
    
    echo "$fs_name -> Ocupa $size_with_suffix, ocupado $percentage_occupied%"
}