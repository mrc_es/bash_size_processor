#!/usr/bin/env bash

extract_pathname() {
    local -r record="$1"
    local -r filename_regex='(?<=\/).*(?=\/)'
    
    local -r filename_match="$(echo "$record" | grep -Po "$filename_regex")"
    local -r filename="/$filename_match/"
    echo "$filename"
}
