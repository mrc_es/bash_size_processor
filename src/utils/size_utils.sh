#!/usr/bin/env bash

normalize_size_to_bytes() {
    local -r size="$1"
    local -r normalized_suffix="$2"
    local -r bytes="$(echo "${size}${normalized_suffix}" | numfmt --from=auto)"
    echo "$bytes"
}
normalize_size_suffix() {
    local -r size_suffix="$1"
    local -r first_size_letter="${size_suffix:0:1}"
    local -ru normalized_size_suffix="$first_size_letter"
    echo "$normalized_size_suffix"
}
extract_size_part() {
    local -r record="$1"
    local -r size_regex='(?<=\().*(?=\))'
    
    local -r size_part="$(echo "$record" | grep -Po "$size_regex")"
    echo "$size_part"
}
extract_size_number() {
    local -r size_part="$1"
    local -r number_regex='\d+'
    
    local -ri size_number="$(echo "$size_part" | grep -Po "$number_regex")"
    echo "$size_number"
}
extract_size_suffix() {
    local -r size_part="$1"
    local -r suffix_regex='[^\d ]+'
    
    local -r size_number="$(echo "$size_part" | grep -Po "$suffix_regex")"
    echo "$size_number"
}
calculate_percentage_per_size() {
    local -ri size_in_bytes="$1"
    local -ri percentage="$((size_in_bytes*100/total_bytes))"
    echo "$percentage"
}
create_size_with_suffix_normalized() {
    local -r row="$1"
    local -r size_part="$(extract_size_part "$row")"
    local -r size_suffix="$(extract_size_suffix "$size_part")"
    local -ri size_number="$(extract_size_number "$size_part")"
    local -r normalized_size_suffix="$(normalize_size_suffix "$size_suffix")"
    
    echo "${size_number}${normalized_size_suffix}"
}
get_bytes_per_row() {
    local -r row="$1"
    local -r size_part="$(extract_size_part "$row")"
    
    local -ri size_number="$(extract_size_number "$size_part")"
    local -r size_suffix="$(extract_size_suffix "$size_part")"
    
    local -r normalized_size_suffix="$(normalize_size_suffix "$size_suffix")"
    
    local -ri total_bytes="$(normalize_size_to_bytes "$size_number" "$normalized_size_suffix")"
    echo "$total_bytes"
}