#!/usr/bin/env bash

readonly _prog_path="$(realpath -e $0)"
readonly _src_path="$(dirname $_prog_path)"
readonly _base_name="$_src_path/.."

source "${_src_path}/utils/fs_utils.sh"
source "${_src_path}/utils/size_utils.sh"
source "${_src_path}/utils/path_utils.sh"
source "${_src_path}/options_reader.sh"

process_row() {
    local -r row="$1"
    
    local -r path_name="$(extract_pathname "$row")"
    local -r size_with_suffix="$(create_size_with_suffix_normalized "$row")"
    local -ri size_in_bytes="$(normalize_size_to_bytes "$size_with_suffix")"
    local -ri percentage="$(calculate_percentage_per_size "$size_in_bytes")"
    
    ((percentage_occupied = percentage_occupied + percentage))
    
    local -r processed_row="$path_name (${size_with_suffix}), ocupa el ${percentage}% del FS $fs_name"
    result_info+=("$processed_row")  
}
process_file() {
    declare -r _file="$1"
    declare -i percentage_occupied=0
    declare -a result_info
    declare -r first_row="$(head -n 1 "$_file")"
    declare -r fs_name="$(extract_fsname "$first_row")"
    declare -ri total_bytes="$(get_bytes_per_row "$first_row")"

    set +m
    shopt -s lastpipe
    
    tail -n +2 "$_file" \
    | while read -r row || [ -n "$row" ]; do
        process_row "$row"
    done 
    
    print_fs_info "$first_row" "$percentage_occupied"
    printf '%s\n' "${result_info[@]}"
}

main() {
    options_reader $@
    declare -r _file="${_base_name}/resources/${_filename}"
    process_file "$_file"
}

main $@
