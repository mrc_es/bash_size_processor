#!/usr/bin/env bash

options_reader() {
	while [ $# -gt 0 ]
	do
		case "$1" in
			--filename | -f )
				shift
				export _filename="$1"
				shift
				;;
			--filename=* )
				export _filename="${1#*=}"
				shift
				;;
			*)	
				shift
				;;
		esac
	done
}